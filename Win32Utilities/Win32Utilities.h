#pragma once

#include <Windows.h>

#include <vector>
#include <string>

namespace WinUtilities
{
	class FileIO
	{
	public:
		static void OpenFile(char *Extension, char *Filter, std::string &OutputPath);
		static void OpenFiles(char *Extension, char *Filter, std::string &Path, std::vector<std::string> &OutputPaths);
	};

	class FileSystem
	{
	public:
		static void GetDirectoryContents(std::string dirctory, std::string filter, std::vector<std::string> &Contents);

		static char* GetFileExt(char *File);
		static std::string GetEXEPath();
		static std::string GetEXEDirectory();
		static std::string GetFileDirectory(std::string file);

	};

	class Dialogs
	{
	public:
		static int MessageAlert(char *header, char *message, bool playSound);
	};
}