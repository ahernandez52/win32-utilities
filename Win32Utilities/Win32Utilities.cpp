#include "Win32Utilities.h"

#include <windowsx.h>
#include <Shlwapi.h>

#pragma comment(lib, "Shlwapi.lib")

using namespace std;

namespace WinUtilities
{
	enum WindowsResponse
	{
		WR_OK		= IDOK,
		WR_CANCEL	= IDCANCEL,
		WR_ABORT	= IDABORT,
		WR_RETRY	= IDRETRY,
		WR_IGNORE	= IDIGNORE,
		WR_YES		= IDYES,
		WR_NO		= IDNO,
	};

	void FileIO::OpenFile(char *Extension, char *Filter, string &OutputPath)
	{
		OPENFILENAME ofn;

		char OutputPathBuffer[256];

		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = NULL;
		ofn.lpstrFile = OutputPathBuffer;
		ofn.lpstrFile[0] = '\0';
		ofn.nMaxFile = 256;
		ofn.lpstrDefExt = Extension;
		ofn.lpstrFilter = Filter;
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

		GetOpenFileName(&ofn);

		OutputPath = OutputPathBuffer;

		//return ofn;
	}

	void FileIO::OpenFiles(char *Extension, char *Filter, string &Path, vector<string> &OutputPaths)
	{
		OPENFILENAME ofn;

		char OutputPathBuffer[256];

		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = NULL;
		ofn.lpstrFile = OutputPathBuffer;
		ofn.lpstrFile[0] = '\0';
		ofn.nMaxFile = 256;
		ofn.lpstrDefExt = Extension;
		ofn.lpstrFilter = Filter;
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		//ofn.nFileExtension = 0;
		ofn.nFileOffset = 0;
		ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT;

		GetOpenFileName(&ofn);
	
		Path = ofn.lpstrFile;

		string file(ofn.lpstrFile + ofn.nFileOffset);

		int offset = ofn.nFileOffset;

		while(file.size() > 0)
		{
			offset += file.size() + 1;
			OutputPaths.push_back(file);
			file = string(ofn.lpstrFile + offset);
		}
	}






	void FileSystem::GetDirectoryContents(string directory, string filter, vector<string> &contents)
	{
		WIN32_FIND_DATA ffd;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		string spec = directory + "\\" + filter;

		hFind = FindFirstFile(spec.c_str(), &ffd);

		do {
			if((string(ffd.cFileName) != ".") && (string(ffd.cFileName) != "..")){
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					//ignore files
					contents.push_back(directory + "\\" + string(ffd.cFileName));
				}
				else 
				{
					contents.push_back(directory + "\\" + string(ffd.cFileName)); 
				}
			}
		} while(FindNextFile(hFind, &ffd) != 0);

		FindClose(hFind);
	}

	char* FileSystem::GetFileExt(char *File)
	{
		return PathFindExtension(File);
	}

	string FileSystem::GetEXEPath()
	{
		vector<char> path(MAX_PATH);
		DWORD result = GetModuleFileName(nullptr, &path[0], static_cast<DWORD>(path.size()));
		return string(path.begin(), path.begin() + result);
	}

	string FileSystem::GetEXEDirectory()
	{
		return GetFileDirectory(GetEXEPath());
	}

	string FileSystem::GetFileDirectory(string file)
	{
		const size_t last_slash_idx = file.rfind('\\');

		if(string::npos != last_slash_idx)
		{
			return file.substr(0, last_slash_idx);
		}

		return "";
	}






	int Dialogs::MessageAlert(char *header, char *message, bool playSound)
	{
		if(playSound) MessageBeep(MB_ICONERROR);

		int result = MessageBox(0, message, header, MB_YESNOCANCEL | MB_ICONEXCLAMATION);

		return result;
	}
}